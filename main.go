package main

import (
	"encoding/csv"
	"flag"
	"io"
	"log/slog"
	"os"
	"strings"

	"github.com/pkg/errors"
)

var dir string
var pathToReplacements string
var skipHidden bool
var isSafeReplacement bool

func init() {
	flag.StringVar(&dir, "dir", "", "Directory with files to make replacements in")
	flag.StringVar(&pathToReplacements, "replacements", "", "File with replacements")
	flag.BoolVar(&skipHidden, "skip-hidden", true, "Skip hidden files")
	flag.BoolVar(&isSafeReplacement, "safe", true, "Unsafe replacement will modify file in place")
	flag.Parse()
}

func main() {
	logger := slog.New(slog.NewJSONHandler(os.Stdout, nil))

	if len(dir) == 0 {
		logger.Error("Please specify directory with files to make replacements in")
		return
	}

	if len(pathToReplacements) == 0 {
		logger.Error("Please specify file with replacements")
		return
	}

	replacements, err := readReplacements(pathToReplacements)
	if err != nil {
		logger.Error("Failed to read replacements", "filename", pathToReplacements, "error", err)
		return
	}

	entries, err := os.ReadDir(dir)
	if err != nil {
		logger.Error("Failed to open directory", "directory", dir, "error", err)
		return
	}

	for _, e := range entries {
		if e.IsDir() {
			continue
		}

		if skipHidden && e.Name()[0] == '.' {
			continue
		}

		filename := dir + "/" + e.Name()
		logger.Info("Processing file", "filename", filename)
		err := replaceInFile(filename, replacements, isSafeReplacement)
		if err != nil {
			logger.Warn("Failed to make replacements in file", "filename", filename, "error", err)
		}
	}
}

func readReplacements(path string) (map[string]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	lines, err := csv.NewReader(file).ReadAll()
	if err != nil {
		return nil, errors.Wrap(err, "failed to read csv file")
	}

	replacements := make(map[string]string)

	for _, l := range lines {
		if len(l) != 2 {
			return nil, errors.New("invalid line in csv file")
		}

		replacements[l[0]] = l[1]
	}

	return replacements, nil
}

func replaceInFile(filename string, replacements map[string]string, safe bool) error {
	file, err := os.OpenFile(filename, os.O_RDWR, 0644)
	if err != nil {
		return errors.Wrap(err, "failed to open file")
	}

	defer file.Close()

	data, err := io.ReadAll(file)
	if err != nil {
		return errors.Wrap(err, "failed to read file")
	}

	content := string(data)

	for k, v := range replacements {
		content = strings.ReplaceAll(content, k, v)
	}

	if safe {
		err = saveModifiedFileSafe(file, content)
		if err != nil {
			return errors.Wrap(err, "failed to save modified file")
		}
	} else {
		err = saveModifiedFileUnsafe(file, content)
		if err != nil {
			return errors.Wrap(err, "failed to save modified file")
		}
	}

	return nil
}

func saveModifiedFileSafe(file *os.File, content string) error {
	tmpFilename := file.Name() + ".tmp"
	bakFilename := file.Name() + ".bak"

	fileStat, err := file.Stat()
	if err != nil {
		return errors.Wrap(err, "failed to get file stat")
	}

	tmpFile, err := os.Create(tmpFilename)
	if err != nil {
		return errors.Wrap(err, "failed to create temporary file")
	}

	err = tmpFile.Chmod(fileStat.Mode())
	if err != nil {
		return errors.Wrap(err, "failed to change temporary file permissions")
	}

	_, err = tmpFile.WriteString(content)
	if err != nil {
		return errors.Wrap(err, "failed to write result to the temporary file")
	}

	err = tmpFile.Sync()
	if err != nil {
		return errors.Wrap(err, "failed to sync temporary file")
	}

	err = os.Rename(file.Name(), bakFilename)
	if err != nil {
		return errors.Wrap(err, "failed to rename original file")
	}

	err = os.Rename(tmpFilename, file.Name())
	if err != nil {
		return errors.Wrap(err, "failed to rename temporary file")
	}

	err = os.Remove(bakFilename)
	if err != nil {
		return errors.Wrap(err, "failed to remove backup file")
	}

	return nil
}

func saveModifiedFileUnsafe(file *os.File, content string) error {
	err := file.Truncate(0)
	if err != nil {
		return errors.Wrap(err, "failed to truncate file")
	}

	_, err = file.Seek(0, 0)
	if err != nil {
		return errors.Wrap(err, "failed to seek to the beginning of the file")
	}

	_, err = file.WriteString(content)
	if err != nil {
		return errors.Wrap(err, "failed to write result to the file")
	}

	return nil
}
